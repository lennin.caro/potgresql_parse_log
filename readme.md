### Configuración del log en PostgreSQL 12+

```bash
log_statement = 'all'
log_directory = 'pg_log'
log_filename = 'postgresql-%Y-%m-%d_%H%M%S.log'
logging_collector = on
log_min_error_statement = error
#log_destination = 'csvlog'
#log_min_messages = 'DEBUG5'
log_min_messages = 'INFO'
logging_collector = on
log_checkpoints = on
log_connections = on
log_disconnections = on
log_lock_waits = on
log_temp_files = 0
log_min_duration_statement = 5000
log_line_prefix = '$|$\t%m\t[%p]\t[%b]\t[%v]\t[%x]\t%q[user=%u,db=%d,app=%a,ip=%h, rip=%r]\t'
log_duration = on
```


### Ejecución del script

__Primero se debe aplicar la configuración de logs en el archivo postgresql.conf.__

El script necesita tres parametros que se colocan en orden, los parametros y el orden son:

* log_file: archivo que contiene los logs de postgresl
* result_directory: donde se ubicaran los archivos json y csv
  * Los archivos json contienen la data detallada obtenida
  * Los archivos csv contienen las stadisticas o datos para generar graficos
* delimit_chars: caracter que se usara para dividir las columnas de os archivos csv

Se generan automaticamente un archivo por cada consultas sql "prepared", este archivo llevar el nombre de "query_X" donde X es la posicion de la consulta referente al archivo "pre_executed_querys_count.csv"


#### Ejemplo ejecución del script
```bash
python3 read_log_file_parse.py /tmp/postgresql-2021-10-22_112050.log /tmp '||'
```