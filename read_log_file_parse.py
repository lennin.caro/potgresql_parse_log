import uuid
from collections import Counter
import json
import sys



def get_duration(str_query_id, min_pos, duration_list):
  ret = ''
  for i in duration_list:
    if str_query_id in i['str_query_id'] and i['pos'] > min_pos:
      ret = i['duration']
      break
  return ret

def get_params(str_query_id, min_pos, params_list):
  ret = ''
  for i in params_list:
    if str_query_id in i['str_query_id'] and i['pos'] > min_pos:
      ret = i['details']
      break
  return ret

def get_querys_lines_from_log(log_file):
  data_lines = ''
  with open(log_file, 'r') as fp:
    data_lines = fp.readlines()
  r_lines_new = []
  t_line = ''
  current_line = 0
  # for r_lines in data_lines[1760:1800]:
  for r_lines in data_lines:
    # import pdb; pdb.set_trace()
    current_line += 1
    if r_lines[0:3] == '$|$' and current_line > 1:
      r_lines_new.append(t_line)
      t_line = r_lines
    else:
      t_line += r_lines
  return r_lines_new


def append_results(result_all, str_query_id, data, data_type):
  if str_query_id in result_all.keys():
    result_all[str_query_id][data_type].append(data)
  else:
    result_all[str_query_id] = {
      "result_sql_list": [],
      "result_execute_list": [],
      "duration_list": [],
      "params_list": [],
    }
    result_all[str_query_id][data_type].append(data)

def get_querys_from_lines(log_lines):
  result_sql_list = []
  result_execute_list = []
  duration_list = []
  params_list = []
  result_all = {}
  cnt_pos = 0
  for raw_line in log_lines:
    print(f"Lineas revisadas {cnt_pos} de {len(log_lines)}")
    cnt_pos += 1
    if raw_line.find('statement:') > 0:
      raw_sql = raw_line[raw_line.find('statement:')+10:].replace('\n', '' ).replace('\t', ' ')
      raw_info = raw_line[3:raw_line.find('LOG')]
      result_dict = {
        "pos": cnt_pos,
        "id": str(uuid.uuid4()),
        "str_query_id": raw_info.split('\t')[4],
        "sql": raw_sql.strip(),
        "info": raw_info.split('\t')[6],
        "exec_time": raw_info.split('\t')[1],
      }
      
      result_sql_list.append(result_dict)
      append_results(result_all, result_dict['str_query_id'], result_dict, 'result_sql_list')
    if raw_line.find('execute') > 0:
      raw_sql = raw_line[raw_line.find('execute'):]
      clean_sql = raw_sql[raw_sql.find(':') + 1:].replace('\n', ' ').replace('\t', ' ')
      raw_info = raw_line[3:raw_line.find('LOG')]
      result_dict = {
        "pos": cnt_pos,
        "id": str(uuid.uuid4()),
        "str_query_id": raw_info.split('\t')[4],
        "sql": clean_sql.strip(),
        "info": raw_info.split('\t')[6],
        "exec_time": raw_info.split('\t')[1],
      }
      result_execute_list.append(result_dict)
      append_results(result_all, result_dict['str_query_id'], result_dict, 'result_execute_list')
    if raw_line.find('duration:') > 0:
      raw_duration = raw_line[raw_line.find('duration:')+9:raw_line.find('ms')].replace('\n', '').replace('\t', '').replace(' ','')
      raw_info = raw_line[3:raw_line.find('LOG')]
      result_dict = {
        "pos": cnt_pos,
        "id": str(uuid.uuid4()),
        "str_query_id": raw_info.split('\t')[4],
        "duration": raw_duration,
        "info": raw_info.split('\t')[6],
      }
      duration_list.append(result_dict)
      append_results(result_all, result_dict['str_query_id'], result_dict, 'duration_list')
    if raw_line.find('DETAIL:') > 0:
      raw_detail = raw_line[raw_line.find('parameters:')+11:].replace('\n', ' ').replace('\t', ' ')
      raw_info = raw_line[3:raw_line.find('DETAIL:')]
      result_dict = {
        "pos": cnt_pos,
        "id": str(uuid.uuid4()),
        "str_query_id": raw_info.split('\t')[4],
        "details": raw_detail.replace("'", "").replace('"', ''),
        "info": raw_info.split('\t')[6],
      }
      params_list.append(result_dict)
      append_results(result_all, result_dict['str_query_id'], result_dict, 'params_list')
  result_sql_list_cnt = 0
  for i in result_sql_list:
    print(f"Obteniendo la duración de las consultas {result_sql_list_cnt} de {len(result_sql_list)}")
    result_sql_list_cnt += 1
    # i['duration'] = get_duration(i['str_query_id'], i['pos'], duration_list)
    duration = [x['duration'] for x in result_all[i['str_query_id']]['duration_list'] if x['pos'] > i['pos']]
    if len(duration) > 0:
      i['duration'] = duration[0]
    else:
      i['duration'] = ''

  result_execute_list_cnt = 0
  for i in result_execute_list:
    print(f"Obteniendo la duración de las consultas preparadas {result_execute_list_cnt} de {len(result_execute_list)}")
    result_execute_list_cnt += 1
    # i['duration'] = get_duration(i['str_query_id'], i['pos'], duration_list)
    # i['params'] = get_params(i['str_query_id'], i['pos'], params_list)
    duration = [x['duration'] for x in result_all[i['str_query_id']]['duration_list'] if x['pos'] > i['pos']]
    if len(duration) > 0:
      i['duration'] = duration[0]
    else:
      i['duration'] = ''
    params = [x['details'] for x in result_all[i['str_query_id']]['params_list'] if x['pos'] > i['pos']]
    if len(params) > 0:
      i['params'] = params[0]
    else:
      i['params'] = ''
    

  return result_sql_list, result_execute_list

def get_statistic_data(result_sql_list, result_execute_list):  
  pre_executed_querys_list = [x['sql'] for x in result_execute_list]
  executed_querys_list = [x['sql'] for x in result_sql_list]
  pre_executed_querys_count = dict(Counter(pre_executed_querys_list))
  executed_querys_count = dict(Counter(executed_querys_list))
  return pre_executed_querys_count, executed_querys_count

def save_counts_in_file(pre_executed_querys_count, executed_querys_count, result_directory, delimit_chars):
  with open(result_directory + '/pre_executed_querys_count.csv', 'w') as fp:
    for k,v in pre_executed_querys_count.items():
      # print("'"+k.strip("'")+"'||'"+str(v)+"'")
      fp.write("'"+k.strip("'")+"'"+delimit_chars+"'"+str(v)+"'"+"\n")

  with open(result_directory + '/executed_querys_count.csv', 'w') as fp:
    for k,v in executed_querys_count.items():
      # print("'"+k.strip("'")+"'||'"+str(v)+"'")
      fp.write("'"+k.strip("'")+"'"+delimit_chars+"'"+str(v)+"'"+"\n")

def save_json_data(result_sql_list, result_execute_list, pre_executed_querys_count, executed_querys_count, result_directory):
  with open(result_directory + '/result_sql.json', 'w') as fp:
    fp.write(json.dumps(result_sql_list))
  
  with open(result_directory + '/result_execute.json', 'w') as fp:
    fp.write(json.dumps(result_execute_list))
  
  with open(result_directory + '/pre_executed_querys_count.json', 'w') as fp:
    fp.write(json.dumps(pre_executed_querys_count))

  with open(result_directory + '/executed_querys_count.json', 'w') as fp:
    fp.write(json.dumps(executed_querys_count))

def save_statistic_from_query(result_list, result_count_dict, result_directory, delimit_chars):
  # result_list es la lista detalada do los sql ejecutados
  # result_count_dict es el diccionario con el total de ejecuciones realizadas por cada query
  query_count = 0
  for k,_ in result_count_dict.items():
    query_count += 1
    filter = k
    ret_filter = [[x['exec_time'].split(' ')[0], x['exec_time'].split(' ')[1], x['duration']] for x in result_list if x['sql'] == filter]
    with open(result_directory + '/query' + str(query_count) + '.csv', 'w') as fp:
      for i in ret_filter:
        fp.write(delimit_chars.join(i).replace('.',',')+"\n")

def main(log_file, result_directory, delimit_chars):
  print("Obteniendo las lineas a revisar")
  querys_lines_from_log = get_querys_lines_from_log(log_file)
  print("Obteniendo las consultas de las lineas")
  result_sql_list, result_execute_list = get_querys_from_lines (querys_lines_from_log)
  print("Generando la data estadistica")
  pre_executed_querys_count, executed_querys_count = get_statistic_data(result_sql_list, result_execute_list)
  print("Guardando los datos")
  save_counts_in_file(pre_executed_querys_count, executed_querys_count, result_directory, delimit_chars)
  save_json_data(result_sql_list, result_execute_list, pre_executed_querys_count, executed_querys_count, result_directory)
  save_statistic_from_query(result_execute_list, pre_executed_querys_count, result_directory, delimit_chars)

if __name__ == '__main__':
  # import pdb; pdb.set_trace()
  log_file = sys.argv[1]
  result_directory = sys.argv[2]
  delimit_chars = sys.argv[3].strip()
  print("Ejecutando los procesos")
  main(log_file, result_directory, delimit_chars)
  print(f"terminado el resultado esta en {result_directory}")


# Ejemplo:
# python3 main.py /home/usuario/Documentos/fullonnet/rdp_windows_share/postgresq_logs/postgresql-2021-10-22_112050.log /tmp '||'