#!/usr/bin/python3
# -*- coding: utf-8 -*-
# import sys

# sys.path += __path__
# =====================================================
# init
# =====================================================


def init(cont):
    '''
    This member function does nothing. It is a hook
    to be called by the BGE to trigger the initialization.
    '''
    pass
